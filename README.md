# Polatouche : installeur pour Composer

Cet installeur (plugin pour Composer) charge les paquets de type `polatouche/plugin-xxx`
dans le répertoire `plugins/` (au lieu du répertoire `vendor/...`)

