<?php
namespace Polatouche\Composer\Installers;

use Composer\Package\PackageInterface;
use Composer\Installer\LibraryInstaller;

class PolatoucheInstaller extends LibraryInstaller{

	const PLUGIN_STARTNAME = 'polatouche/plugin-';

	/**
	 * {@inheritDoc}
	 */
	public function getInstallPath(PackageInterface $package) {

		if (strpos($package->getPrettyName(), self::PLUGIN_STARTNAME) !== 0) {
			throw new \InvalidArgumentException(
				'Unable to install plugin, Polatouche plugins '
				. 'should always start their package name with '
				. '"' . self::PLUGIN_STARTNAME . '"'
			);
		}

		return 'plugins/'.substr($package->getPrettyName(), strlen(self::PLUGIN_STARTNAME));
	}

	/**
	 * {@inheritDoc}
	 */
	public function supports($packageType) {
		return 'polatouche-plugin' === $packageType;
	}
}

