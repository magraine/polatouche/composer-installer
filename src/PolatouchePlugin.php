<?php

namespace Polatouche\Composer\Installers;

use Composer\Composer;
use Composer\IO\IOInterface;
use Composer\Plugin\PluginInterface;

class PolatouchePlugin implements PluginInterface
{
	public function activate(Composer $composer, IOInterface $io)
	{
		$installer = new PolatoucheInstaller($io, $composer);
		$composer->getInstallationManager()->addInstaller($installer);
	}
}